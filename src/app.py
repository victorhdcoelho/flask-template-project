# Python libraries
import os

# External libraries
from flask import (
        Flask,
        jsonify,
        make_response
        )
from django.apps import apps
from django.conf import settings

apps.populate(settings.INSTALLED_APPS)

# from api.models import Hit

SECRET_KEY = os.environ.get("SECRET_KEY", "")
app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY


@app.route('/Hit/')
def test():
    return str(Hit.objects.count())


@app.route('/')
def init():
    return make_response(
            jsonify(
                {
                    "message": "APP: {} STATUS: RUNNING DB: {}".format(
                        os.environ.get("APP_NAME", "SemNome"),
                        os.environ["DATABASE_URL"]
                        )
                }
                ),
            200
        )


if __name__ == "__main__":
    app.run(
        debug=os.environ.get("DEBUG", True),
        host=os.environ.get("APP_HOST", "0.0.0.0"),
        port=os.environ.get("FLASK_INTERNAL_PORT", "8889")
    )
